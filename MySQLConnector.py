import mysql.connector as mc
from datetime import datetime
from dateutil import relativedelta
import my_credentials

db_index = "4"


def get_tag_id(hostname, password, user, db_name, tag_path, port=3306):

    db = mc.connect(host=hostname, password=password, user=user, database=db_name, port=port)
    query = generic_select_query("id", "sqlth_te", 'tagpath="{}" '.format(tag_path), db)

    events = []
    for (tagid) in query:
        events.append(tagid[0])

    return events


def query_tag_history(hostname, password, user, database, table, tagid, start_time, end_time, port=3306):

    db = mc.connect(host=hostname, password=password, user=user, database=database, port=port)
    query = generic_select_query("tagid, intvalue, floatvalue, stringvalue, t_stamp", table,
                                 "tagid={} AND t_stamp BETWEEN {} AND {}".format(tagid, start_time, end_time), db)

    events = []
    for (tagid, intvalue, floatvalue, stringvalue, t_stamp) in query:

        millistodate = (datetime.fromtimestamp(int(t_stamp)/1000).strftime('%d/%m/%Y %H:%M:%S,%f')).split(",")[0]
        events.append([millistodate, t_stamp, intvalue, floatvalue, stringvalue])
    return events


def generic_select_query(selectstring, table, wherecondition, dbconnection):

    query = "SELECT {} FROM {} WHERE {}".format(selectstring, table, wherecondition)
    cursor = dbconnection.cursor()
    cursor.execute(query)
    events = []
    for event in cursor:
        events.append(event)
    cursor.close()
    dbconnection.close()

    return events
   

def query_tag_history_by_table(hostname, password, user, db_name, table, tag_path, d1_start, d2_end, port=3302):

    tag_ids = get_tag_id(hostname, password, user, db_name, tag_path,port)
    events = []

    for tagId in tag_ids:
        # print tagId
        # print table
        a = query_tag_history(hostname, password, user, db_name, table, tagId, d1_start, d2_end, port)
        for event in a:
            events.append(event)

    return events


def query_tag_lz_history(hostname, user, password, db_name, tag_path, d1_millis, d2_millis, port=3302):

    hist_start = d1_millis
    hist_end = d2_millis
    list_of_dates = list_ym_between_millis_dates(hist_start, hist_end)

    events = []
    for dateYM in list_of_dates:
        print dateYM
        events += query_tag_history_by_table(hostname, password, user, db_name,
                                             "sqlt_data_"+db_index+"_" + str(dateYM), tag_path, hist_start, hist_end, port)
  
    return events


def get_all_tags(hostname, password, user, db_name, port=3306):

    db = mc.connect(host=hostname, password=password, user=user, database=db_name, port=port)
    query = generic_select_query("tagpath", "sqlth_te", '', db)

    events = []
    for (tagid) in query:
        events.append(tagid[0])

    return events


def list_ym_between_millis_dates(d1_millis, d2_millis):

    d1 = datetime.fromtimestamp(d1_millis/1000)
    d2 = datetime.fromtimestamp(d2_millis/1000)
    n_months = int(relativedelta.relativedelta(d2, d1).months)

    months_list = [str(d1.year)+"_"+str("%02d" % (d1.month,))]
    year_add = 0
    for i in range(n_months):
        add_months = d1.month+i+1
        if d1.month+i > 12:
            year_add += 1
            add_months -= year_add*12

        months_list.append(str(d1.year+year_add)+"_"+str("%02d" % (add_months,)))
    months_list.append(str(d2.year)+"_"+str("%02d" % (d2.month,)))
    return months_list


# list_ym_between_millis_dates(1558639388692, 1573665993383)
start_millis = 1568639388692
end_millis = 1573665993383

host = "s-db2vm-data.online.luxzeplin.org"
password = my_credentials.db_password
user = my_credentials.db_user
db_name = "ig_history"
tag_path = "LZ System Test/Analog Electronics/iBoot Rack 4/Temperature 2/Value"
lines = query_tag_lz_history(host, user, password, db_name, tag_path, start_millis, end_millis, 3306)
new_lines = ""

for l in lines:
    new_l = " ".join([str(v) for v in l])
    new_l += "\n"
    new_lines += new_l
f = open("results.txt", "w")
f.write(new_lines)